Module provides simple and fast check for Drupageddon exploits seen in
the wild for admins without Drush and SSH. 

USAGE:
http://example.com/admin/config/development/test_drupageddon

Based on drupalgeddon-7.x-1.x-dev.
https://www.drupal.org/project/drupalgeddon

Module tests:
 - menu_router table
 - user names
 - Drupal core 7.32 unknown php file names (except sites/* folders!)

Recomendation:
Check contents of all *.php files for string "eval(" and "@$_COOKIE[" 
 via FTP.

@author 
	Martin Klíma, martin.klima@hqis.cz
	https://www.drupal.org/u/martin_klima
